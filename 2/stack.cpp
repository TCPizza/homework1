#include <iostream>
#include <stdio.h>
#include "stack.h"

void push(stack* s, unsigned int element)
{
	stack* stackNode = newNode(element);
	stackNode->next = new stack;
	*stackNode->next = *s;
	s->data = stackNode->data;
	s->next = stackNode->next;
	std::cout << ("%d pushed to stack\n", element) << std::endl;
}

int pop(stack* s)
{
	int popped = 0;
	if (s->next == NULL)
	{
		return -1;
	}
	stack tmp = *s;
	s->data = s->next->data;
	s->next = s->next->next;
	popped = tmp.data;
	delete tmp.next;
	return popped;
}

stack* newNode(int data)
{
	stack* stackNode = new stack;
	stackNode->data = data;
	stackNode->next = NULL;
	return stackNode;
}

void initStack(stack* s)
{
	s->data = 0;
	s->next = NULL;
}

void cleanStack(stack* s)
{
	while (pop(s) != -1)
	{
		continue;
	}
}
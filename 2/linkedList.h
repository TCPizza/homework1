#pragma once

typedef struct linkedList
{
	int val;
	struct linkedList *next;

} linkedList;

void add(linkedList *head, int val);

void removeList(linkedList *head);
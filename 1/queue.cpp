#include <iostream>
#include "queue.h"


void initQueue(queue* q, unsigned int size)
{
	q->capacity = size;
	q->front = q->size = 0;
	q->rear = q->capacity - 1;
	q->array = new int[q->capacity];
}

void cleanQueue(queue* q)
{
	delete[](q->array);
	delete(q);
}

void enqueue(queue* q, unsigned int newValue)
{
	if (q->size == q->capacity - 1)
	{
		std::cout<<("\nQueue is Full!!")<<std::endl;
	}
		
	else 
	{
		q->rear = (q->rear + 1) % q->capacity;
		q->array[q->rear] = newValue;
		q->size = q->size + 1;
	}
	
}

int dequeue(queue* q)
{
	if (q->front == -1)
	{
		std::cout<<("\nQueue is Empty!!")<<std::endl;
		return -1;
	}
		
	else {
		int item = q->array[q->front];
		q->front = (q->front + 1) % q->capacity;
		q->size = q->size - 1;
		return item;
	}
	
}
